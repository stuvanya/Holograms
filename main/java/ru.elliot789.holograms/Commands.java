package ru.elliot789.holograms;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static ru.elliot789.holograms.Settings.holograms_cfg;

public class Commands implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        // COMMAND "BB"
        if (cmd.getName().equalsIgnoreCase("eholograms")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage("Нельзя писать с консоли");
                return false;
            }

            Player player = (Player) sender;

            if (!player.hasPermission("holograms.admin")) {
                player.sendMessage("§cНет прав.");
                return false;
            }

            // HELP MENU
            if (args.length == 0) {
                player.sendMessage("§e----------------- [Holograms] -----------------");
                player.sendMessage("§3/ehd reload §7- перезагрузить");
                player.sendMessage("§3/ehd list [lang] §7- список голограмм");
                player.sendMessage("§3/ehd create [lang][имя] §7- создать голограмму");
                player.sendMessage("§3/ehd remove [lang][имя] §7- удалить голограмму");
                player.sendMessage("§3/ehd setloc [lang][имя] §7- установить локацию голограммы");
                player.sendMessage("§3/ehd tp [lang][имя] §7- тп к голограмме");
                player.sendMessage("§3/ehd addline [lang][имя][текс] §7- добавить текст (Если добавить предмет, то item:name");
                player.sendMessage("§3/ehd editline [lang][имя][строчка][текс] §7- изменить текст");
                return true;
            }

            if (args[0].equalsIgnoreCase("reload")) {
                if (args.length == 1) {
                    Utils.reloadHologram();
                    player.sendMessage("Тип ок");
                    return true;
                }
                player.sendMessage("§cУ этой команды 1 аргумент");
                return false;
            }

            if (args[0].equalsIgnoreCase("list")) {
                if (args.length == 2) {
                    FileConfiguration file = holograms_cfg.get(args[1]);

                    file.getConfigurationSection("Holograms").getKeys(false).forEach(player::sendMessage);
                    return true;
                }
                player.sendMessage("§cУ этой команды 1 аргумент");
                return false;
            }

            if (args[0].equalsIgnoreCase("create")) {
                if (args.length == 3) {
                    FileConfiguration file = holograms_cfg.get(args[1]);
                    String loc = player.getLocation().getWorld().getName() + ", " + player.getLocation().getX() + ", " + player.getLocation().getY() + ", " + player.getLocation().getZ();

                    file.createSection("Holograms." + args[2]);
                    file.set("Holograms." + args[2] + ".location", loc);

                    player.sendMessage("§aГолограмма создана.");
                    saveFile(args[1]);

                    Utils.reloadHologram();
                    return true;
                }
                player.sendMessage("§cУ этой команды 2 аргумента");
                return false;
            }

            if (args[0].equalsIgnoreCase("remove")) {
                if (args.length == 3) {
                    FileConfiguration file = holograms_cfg.get(args[1]);

                    file.set("Holograms." + args[2], null);

                    player.sendMessage("§aГолограмма удалена.");
                    saveFile(args[1]);

                    Utils.reloadHologram();
                    return true;
                }
                player.sendMessage("§cУ этой команды 2 аргумента");
                return false;
            }

            if (args[0].equalsIgnoreCase("setloc")) {
                if (args.length == 3) {
                    FileConfiguration file = holograms_cfg.get(args[1]);
                    String loc = player.getLocation().getWorld().getName() + ", " + player.getLocation().getX() + ", " + player.getLocation().getY() + ", " + player.getLocation().getZ();

                    file.set("Holograms." + args[2] + ".location", loc);

                    player.sendMessage("§aТочка установлена.");
                    saveFile(args[1]);

                    Utils.reloadHologram();
                    return true;
                }
                player.sendMessage("§cУ этой команды 2 аргумента");
                return false;
            }

            if (args[0].equalsIgnoreCase("tp")) {
                if (args.length == 3) {
                    FileConfiguration file = holograms_cfg.get(args[1]);

                    String[] loc = file.getString("Holograms." + args[2] + ".location").split(", ");
                    Location l = new Location(
                            Bukkit.getWorld(loc[0]),
                            Double.parseDouble(loc[1]),
                            Double.parseDouble(loc[2]),
                            Double.parseDouble(loc[3]));

                    player.teleport(l);
                    player.sendMessage("Вы успешно телепортировалисб");
                    return true;
                }
                player.sendMessage("§cУ этой команды 2 аргумента");
                return false;
            }

            if (args[0].equalsIgnoreCase("addline")) {
                if (args.length >= 3) {
                    FileConfiguration file = holograms_cfg.get(args[1]);

                    List<String> line = new ArrayList<>(file.getStringList("Holograms." + args[2] + ".lines"));

                    StringBuilder text = new StringBuilder();
                    for (int i = 4; i <= args.length; i++) {
                        if (args.length != i) {
                            text.append(args[i - 1]).append(" ");
                        } else {
                            text.append(args[i - 1]);
                        }
                    }
                    line.add(text.toString());

                    file.set("Holograms." + args[2] + ".lines", line);

                    saveFile(args[1]);
                    Utils.reloadHologram();

                    player.sendMessage("Вы успешно добавили текст");
                    return true;
                }
                player.sendMessage("§cУ этой команды 4 аргумента");
                return false;
            }

            if (args[0].equalsIgnoreCase("editline")) {
                if (args.length >= 3) {
                    FileConfiguration file = holograms_cfg.get(args[1]);

                    List<String> line = new ArrayList<>(file.getStringList("Holograms." + args[2] + ".lines"));

                    StringBuilder text = new StringBuilder();
                    for (int i = 5; i <= args.length; i++) {
                        if (args.length != i) {
                            text.append(args[i - 1]).append(" ");
                        } else {
                            text.append(args[i - 1]);
                        }
                    }
                    line.set(Integer.valueOf(args[3]) - 1, text.toString());

                    file.set("Holograms." + args[2] + ".lines", line);

                    saveFile(args[1]);
                    Utils.reloadHologram();

                    player.sendMessage("Вы успешно изменили текст");
                    return true;
                }
                player.sendMessage("§cУ этой команды 4 аргумента");
                return false;
            }
        }
        return false;
    }


    private static void saveFile(String lang) {

        File file = new File(Settings.plugin.getDataFolder() + File.separator + "Lang", lang + ".yml");
        FileConfiguration cfg = holograms_cfg.get(lang);

        try {
            cfg.save(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
