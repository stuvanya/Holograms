package ru.elliot789.holograms;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import com.gmail.filoghost.holographicdisplays.api.VisibilityManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by elliot789
 * Date: 20.01.2018
 * Time: 19:07
 */
class ShowHideHolograms {

    public static Map<Player, List<Hologram>> statsPlayer = new HashMap<>(); // Голограммы со статистикой

    static void showHologramStats(Player p, Map.Entry<String, Hologram> entry, String lang) {
        new BukkitRunnable() {
            @Override
            public void run() {
                FileConfiguration config = Settings.holograms_cfg.get(lang);
                String name = entry.getKey().split("_")[1];

                String sql = config.getString("Holograms." + name + ".sql");
                List<String> columns = config.getStringList("Holograms." + name + ".columns");
                List<String> lines = config.getStringList("Holograms." + name + ".lines");
                String[] loc = config.getString("Holograms." + name + ".location").split(", ");
                Location l = new Location(
                        Bukkit.getWorld(loc[0]),
                        Double.parseDouble(loc[1]),
                        Double.parseDouble(loc[2]),
                        Double.parseDouble(loc[3]));


                int i = 0;
                int linesNumber = 0;
                List<String> top = MySQL.loadStats(p, sql, columns);

                for (String line : lines) {
                    if (line.contains("%")) {
                        int p1 = line.indexOf("%") + 1;
                        int p2 = line.indexOf("%", p1);

                        if (columns.contains(line.substring(p1, p2))) {
                            if (top != null) {
                                lines.set(linesNumber, line.replace("%" + columns.get(i) + "%", top.get(i)));
                            } else {
                                lines.set(linesNumber, line.replace("%" + columns.get(i) + "%", "0"));
                            }
                            i++;
                        } else if (line.contains("%player%")) {
                            lines.set(linesNumber, line.replace("%player%", p.getName()));
                        }
                    }
                    linesNumber++;
                }

                Hologram hologram = HologramsAPI.createHologram(Settings.plugin, l);
                Settings.holograms.put(lang + "_" + entry.getKey().split("_")[1] + "_" + p, hologram);
                if (!hologram.getWorld().getName().equals(p.getWorld().getName())) return;

                for (String s : lines) {
                    hologram.appendTextLine(s.replace("&", "§"));
                }

                VisibilityManager visiblityManager = hologram.getVisibilityManager();
                visiblityManager.setVisibleByDefault(false);
                visiblityManager.showTo(p);

                List<Hologram> list;
                if (statsPlayer.get(p) == null) {
                    list = new ArrayList<>();
                    list.add(hologram);
                    statsPlayer.put(p, list);
                } else {
                    list = statsPlayer.get(p);
                    list.add(hologram);
                    statsPlayer.put(p, list);
                }

            }
        }.runTask(Settings.plugin);
    }

    static void showHologram(Player p, Map.Entry<String, Hologram> test) {
        new BukkitRunnable() {
            @Override
            public void run() {
                //String lang = getLang(p);
                //String langHologram = test.getKey().split("_")[0];
                //String name = test.getKey().split("_")[1];

                //if (!Settings.holograms.containsKey(lang + "_" + name)) lang = getLangD();

                //if (langHologram.equals(lang)) {
                    Hologram h = test.getValue();

                   // if (!h.getWorld().getName().equals(p.getWorld().getName())) return;

                    VisibilityManager visiblityManager = h.getVisibilityManager();
                    visiblityManager.setVisibleByDefault(false);
                    visiblityManager.showTo(p);

               // }
            }
        }.runTask(Settings.plugin);
    }
}
