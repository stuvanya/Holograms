package ru.elliot789.holograms;

import org.bukkit.entity.Player;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import static ru.elliot789.eapi.MySQL.connection;

/**
 * Created by elliot879
 * Date: 19.12.2017
 * Time: 12:08
 */
public class MySQL {

    static Map<String, String> top = new LinkedHashMap<>();

    public static void loadTop(String query) {
        Settings.log("Load top " + query);
        try (PreparedStatement sql = connection.prepareStatement(query)) {
            top.clear();
            try (ResultSet result = sql.executeQuery()) {
                while (result.next()) {
                    top.put(result.getString(1), result.getString(2));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static List<String> loadStats(Player p, String query, List<String> array) {
        Settings.log("Load stats " + query);
        try (PreparedStatement sql = connection.prepareStatement(query)) {
            List<String> stats = new ArrayList<>();

            sql.setString(1, p.getName());

            try (ResultSet result = sql.executeQuery()) {
                if (result.next()) {
                    for (String value : array) {
                        stats.add(result.getString(value));
                    }
                    return stats;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
