package ru.elliot789.holograms;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Settings {

    public static Plugin plugin = Bukkit.getPluginManager().getPlugin("Holograms");
    private static FileConfiguration cfg = Bukkit.getPluginManager().getPlugin("Holograms").getConfig();

    public static Boolean online = cfg.getBoolean("Online.Enable");
    public static int onlineUpdate = cfg.getInt("Online.Update");

    public static Boolean top = cfg.getBoolean("Top.Enable");
    public static int topUpdate = cfg.getInt("Top.Update");

    public static Boolean stats = cfg.getBoolean("Stats.Enable");


    static Map<String, Hologram> holograms = new ConcurrentHashMap<>();
    static Map<String, FileConfiguration> holograms_cfg = new ConcurrentHashMap<>();

    /**
     * Logger
     */
    public static void log(String msg) {
        Bukkit.getLogger().info("[Lobby] " + msg);
    }
}
