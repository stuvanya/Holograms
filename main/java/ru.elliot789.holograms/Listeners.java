package ru.elliot789.holograms;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class Listeners implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        if (Bukkit.getOnlinePlayers().size() >= 1) {
            //Utils utils = new Utils();
            Utils.onHolograms();
            Utils.onItems();
        }
    }

    @EventHandler
    public void onQit(PlayerQuitEvent e) {
        Player p = e.getPlayer();

        Settings.holograms.values().forEach(h -> h.getVisibilityManager().resetVisibility(p));
        Utils.check.remove(p);
        Utils.hologramsPlayer.remove(p.getName()); //Удаляет у него голограммы

        if (ShowHideHolograms.statsPlayer.get(p) != null) {
            ShowHideHolograms.statsPlayer.get(p).forEach(Hologram::delete); //Удаляет у него голограммы
            ShowHideHolograms.statsPlayer.remove(p); //Удаляет у него голограммы
        }

        if (Bukkit.getOnlinePlayers().size() - 1 == 0) {
            Bukkit.getScheduler().cancelTask(Utils.hologram);
            Bukkit.getScheduler().cancelTask(Utils.items);
        }
    }

/*    @EventHandler
    public void onWorld(PlayerChangedWorldEvent e) {
        Player p = e.getPlayer();
        hideHologram(p);
        showHologram(p);
    }*/

/*    @EventHandler
    public void onPickup(PlayerMoveEvent e) {
        Player p = e.getPlayer();

        for (Map.Entry<String, Hologram> entry : holograms.entrySet()) {

            Hologram hologram = entry.getValue();
            String name = entry.getKey().split("_")[1];
            String lang = getLang(p);
            FileConfiguration config = Settings.holograms_cfg.get(entry.getKey().split("_")[0]);
            boolean stats = config.getBoolean("Holograms." + entry.getKey().split("_")[1] + ".stats");

            if (!holograms.containsKey(lang + "_" + name)) lang = getLangD();
            if (!lang.equals(entry.getKey().split("_")[0])) continue; // Если голограмма не совпадает с его языком

            if (name.contains("item")) {
                if (Distance.checkDistanse(p, hologram.getLocation().add(0, -1, 0)) < 0.8) {
                    if (check.get(p) != null) {
                        if (check.get(p).equals(hologram)) continue;
                    }
                    check.put(p, hologram);

                    hologram.getVisibilityManager().hideTo(p);
                    p.playSound(p.getLocation(), Sound.BLOCK_LAVA_POP, 1.0F, 3.0F);
                    ParticleEffect.EXPLOSION_LARGE.display(0.1F, 0.1F, 0.1F, 0.1F, 10, hologram.getLocation().add(0, -2, 0), p);
                } else if (Distance.checkDistanse(p, hologram.getLocation()) > 4.5) {
                    if (check.get(p) != null) {
                        if (check.get(p).equals(hologram)) check.remove(p);
                        hologram.getVisibilityManager().showTo(p);
                    }
                }
            } else if (stats) {
                if (Distance.checkDistanse(p, hologram.getLocation()) < 20) {
                    if (statsPlayer.get(p) != null) {
                        if (statsPlayer.get(p).equals(name)) continue;
                    }
                    statsPlayer.put(p, name);
                    showHologramStats(p, entry); // Грузим его стату, если он рядом с ней
                }
            } else {
                if (Distance.checkDistanse(p, hologram.getLocation()) < 30) {
                    if (hologramsPlayer.get(p) != null)
                        if (hologramsPlayer.get(p).equals(name)) continue;

                    hologramsPlayer.put(p, name);
                    showHologram(p, entry); // Грузим его стату, если он рядом с ней
                }
            }
        }
    }*/
}
