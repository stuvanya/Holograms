package ru.elliot789.holograms;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.VisibilityManager;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;


public class Holograms extends JavaPlugin {

    @Override
    public void onEnable() {
        Bukkit.getServer().getPluginManager().registerEvents(new Listeners(), Settings.plugin);
        getCommand("eholograms").setExecutor(new Commands());

        Utils.loadHolograms();
        startUpdateOnline();
        startUpdateTop();
    }

    @Override
    public void onDisable() {
        for (Hologram h : Settings.holograms.values()) {
            VisibilityManager visiblityManager = h.getVisibilityManager();
            visiblityManager.resetVisibilityAll();
        }
    }

    private void startUpdateOnline() {
        if (!Settings.online) return;
        new BukkitRunnable() {
            @Override
            public void run() {
                Utils.updateOnline();
            }
        }.runTaskTimer(Settings.plugin, 100, 20 * Settings.onlineUpdate);
    }

    private void startUpdateTop() {
        if (!Settings.top) return;
        new BukkitRunnable() {
            @Override
            public void run() {
                Utils.updateTop();
            }
        }.runTaskTimer(Settings.plugin, 100, 20 * Settings.topUpdate);
    }
}