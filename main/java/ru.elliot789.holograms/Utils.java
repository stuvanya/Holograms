package ru.elliot789.holograms;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import com.gmail.filoghost.holographicdisplays.api.VisibilityManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import ru.elliot789.eapi.api.PexFormatter;
import ru.elliot789.eapi.minigames.particle.lib.ParticleEffect;
import ru.elliot789.eapi.utils.location.Distance;
import ru.elliot789.lobby.gui.Gui;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ru.elliot789.eapi.languages.LanguagesAPI.*;
import static ru.elliot789.holograms.Settings.holograms;
import static ru.elliot789.holograms.ShowHideHolograms.showHologram;
import static ru.elliot789.holograms.ShowHideHolograms.showHologramStats;

class Utils {

    public static Map<Player, Hologram> check = new HashMap<>(); // Чекает итем, который валяется где-то
    public static Map<String, List<String>> hologramsPlayer = new HashMap<>(); // Какие голограммы показывает игроку

    public static int hologram = 0;
    public static int items = 0;

    public static void onHolograms() {
        hologram = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(Settings.plugin, () -> {
            for (Player p : Bukkit.getOnlinePlayers()) {
                for (Map.Entry<String, Hologram> entry : holograms.entrySet()) {

                    Hologram hologram = entry.getValue();
                    String langHologram = entry.getKey().split("_")[0];
                    String name = entry.getKey().split("_")[1];
                    String lang = getLang(p);
                    FileConfiguration config = Settings.holograms_cfg.get(entry.getKey().split("_")[0]);
                    boolean stats = config.getBoolean("Holograms." + entry.getKey().split("_")[1] + ".stats");

                    if (!holograms.containsKey(lang + "_" + name)) lang = getLangD();
                    if (!lang.equals(langHologram)) continue; // Если голограмма не совпадает с его языком
                    if (name.contains("item")) continue;

                    VisibilityManager visibility = hologram.getVisibilityManager();

                    if (stats && Settings.stats) {
                        if (Distance.checkDistanse(p, hologram.getLocation()) < 30) {

/*                            if(ShowHideHolograms.statsPlayer.get(p) != null){
                                if(ShowHideHolograms.statsPlayer.get(p).contains(hologram)) continue;
                            }*/

                            if (hologramsPlayer.get(p.getName()) != null) {
                                if (hologramsPlayer.get(p.getName()).contains(name)) {
/*                                    if (!visibilityStats.isVisibleTo(p)) {
                                        visibilityStats.showTo(p);
                                        System.out.println("SHOW STATS " + name);
                                    }*/
                                    continue; // Если уже загруженно
                                }
                            }

                            List<String> list;
                            if (hologramsPlayer.get(p.getName()) == null) {
                                list = new ArrayList<>();
                                list.add(name);
                                hologramsPlayer.put(p.getName(), list);
                            } else {
                                list = hologramsPlayer.get(p.getName());
                                list.add(name);
                                hologramsPlayer.put(p.getName(), list);
                            }

                            //System.out.println("SHOW STATS " + name);
                            showHologramStats(p, entry, lang); // Грузим его стату, если он рядом с ней
                        }/* else {
                            if (visibilityStats.isVisibleTo(p)) {
                                visibilityStats.resetVisibility(p);
                                System.out.println("HIDE HOLOGRAM " + name);
                            }
                        }*/
                    } else {
                        if (Distance.checkDistanse(p, hologram.getLocation()) < 30) {
                            if (hologramsPlayer.get(p.getName()) != null) {
                                if (hologramsPlayer.get(p.getName()).contains(name)) {
                                    if (!visibility.isVisibleTo(p)) {
                                        visibility.showTo(p);
                                        //System.out.println("SHOW HOLOGRAM " + name);
                                    }
                                    continue; // Если уже загруженно
                                }
                            }

                            List<String> list;
                            if (hologramsPlayer.get(p.getName()) == null) {
                                list = new ArrayList<>();
                                list.add(name);
                                hologramsPlayer.put(p.getName(), list);
                            } else {
                                list = hologramsPlayer.get(p.getName());
                                list.add(name);
                                hologramsPlayer.put(p.getName(), list);
                            }

                            //System.out.println("SHOW HOLOGRAM " + name);
                            showHologram(p, entry); // Грузим его стату, если он рядом с ней
                        } else {
                            if (visibility.isVisibleTo(p)) {
                                visibility.resetVisibility(p);
                                //System.out.println("HIDE HOLOGRAM " + name);
                            }
                        }
                    }
                }
            }
        }, 40L, 40L);
    }

	public static void onItems() {
		if (Settings.items == null) {
			Settings.items = true;
		}
		if (Settings.items) {
			items = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(Settings.plugin, () -> {
				for (Player p : Bukkit.getOnlinePlayers()) {
					for (Map.Entry<String, Hologram> entry : holograms.entrySet()) {

						Hologram hologram = entry.getValue();
						String name = entry.getKey().split("_")[1];
						String lang = getLang(p);

						if (!holograms.containsKey(lang + "_" + name))
							lang = getLangD();
						if (!lang.equals(entry.getKey().split("_")[0]))
							continue; // Если голограмма не совпадает с его языком

						VisibilityManager visibility = hologram.getVisibilityManager();

						if (name.contains("item")) {
							if (Distance.checkDistanse(p, hologram.getLocation().add(0, -1, 0)) < 0.8) { // Лопаем итем
								if (check.get(p) != null) {
									if (check.get(p).equals(hologram))
										continue;
								}

								check.put(p, hologram);
								hologram.getVisibilityManager().resetVisibility(p);
								// System.out.println("Скрытие голограммы ITEM");

								p.playSound(p.getLocation(), Sound.BLOCK_LAVA_POP, 1.0F, 3.0F);
								p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 3 * 20, 2, true, false));
								ParticleEffect.EXPLOSION_LARGE.display(0.1F, 0.1F, 0.1F, 0.1F, 10,
										hologram.getLocation().add(0, -2, 0), p);
							} else if (Distance.checkDistanse(p, hologram.getLocation()) < 30
									&& Distance.checkDistanse(p, hologram.getLocation()) > 4.5) {
								if (check.get(p) != null) {
									if (check.get(p).equals(hologram)) {
										check.remove(p);
										hologram.getVisibilityManager().showTo(p);
										// System.out.println("Показал голограмму ITEM");
									}
								}

								if (hologramsPlayer.get(p.getName()) != null) {
									if (hologramsPlayer.get(p.getName()).contains(name)) {
										if (!visibility.isVisibleTo(p)) {
											visibility.showTo(p);
											// System.out.println("SHOW ITEM " + name);
										}
										continue; // Если уже загруженно
									}
								}

								List<String> list;
								if (hologramsPlayer.get(p.getName()) == null) {
									list = new ArrayList<>();
									list.add(name);
									hologramsPlayer.put(p.getName(), list);
								} else {
									list = hologramsPlayer.get(p.getName());
									list.add(name);
									hologramsPlayer.put(p.getName(), list);
								}

								// System.out.println("SHOW ITEM " + name);
								hologram.getVisibilityManager().showTo(p);

							} else if (Distance.checkDistanse(p, hologram.getLocation()) > 30) {
								if (visibility.isVisibleTo(p)) {
									visibility.resetVisibility(p);
									// System.out.println("HIDE ITEM " + name);
								}
							}
						}
					}
				}
			}, 10L, 10L);
		}
	}

    static void loadHolograms() {
        new BukkitRunnable() {
            @Override
            public void run() {
                for (String lang : getLangs()) {
                    File files = new File(Settings.plugin.getDataFolder(), "Lang" + File.separator + lang + ".yml");
                    if (!files.exists()) {
                        continue;
                    }

                    FileConfiguration config = YamlConfiguration.loadConfiguration(files);
                    if (config.getConfigurationSection("Holograms") == null) continue;
                    Settings.holograms_cfg.put(lang, config);

                    for (String name : config.getConfigurationSection("Holograms").getKeys(false)) {
                        try {
                            String[] loc = config.getString("Holograms." + name + ".location").split(", ");
                            Location l = new Location(
                                    Bukkit.getWorld(loc[0]),
                                    Double.parseDouble(loc[1]),
                                    Double.parseDouble(loc[2]),
                                    Double.parseDouble(loc[3]));

                            Hologram hologram = HologramsAPI.createHologram(Settings.plugin, l);
                            List<String> lines = config.getStringList("Holograms." + name + ".lines");

                            for (String s : lines)
                                if (s.contains("item:"))
                                    hologram.appendItemLine(new ItemStack(Material.matchMaterial(s.substring(s.indexOf(":", +1)))));
                                else
                                    hologram.appendTextLine(s.replace("&", "§"));

                            Settings.holograms.put(lang + "_" + name, hologram);

                            VisibilityManager visiblityManager = hologram.getVisibilityManager();
                            visiblityManager.setVisibleByDefault(false);
                            Settings.log("[Holograms] Предмет " + name + " [" + lang + "]" + " успешно загружен ");
                        } catch (Exception ex) {
                            Settings.log("[Holograms] Ошибка при загрузке предмета " + name + " [" + lang + "]" + ". Ошибка - " + ex.getMessage());
                        }
                    }
                }
            }
        }.runTask(Settings.plugin);
    }

    static void reloadHologram() {
        new BukkitRunnable() {
            @Override
            public void run() {
                Bukkit.getOnlinePlayers().forEach(p -> {
                    Settings.holograms.values().forEach(h -> h.getVisibilityManager().resetVisibility(p));
                    check.remove(p);
                    hologramsPlayer.remove(p.getName()); //Удаляет у него голограмму
                });

                Settings.holograms.values().forEach(Hologram::delete); // Удаляем
                Settings.holograms_cfg.clear();
                Settings.holograms.clear();

                loadHolograms();

                if (Settings.online) updateOnline();
                if (Settings.top) updateTop();

            }
        }.runTask(Settings.plugin);
    }

    public synchronized static void updateTop() {
        new BukkitRunnable() {
            @Override
            public void run() {
                for (Map.Entry<String, Hologram> entry : Settings.holograms.entrySet()) {

                    FileConfiguration config = Settings.holograms_cfg.get(entry.getKey().split("_")[0]);

                    boolean stats = config.getBoolean("Holograms." + entry.getKey().split("_")[1] + ".top");
                    String query = config.getString("Holograms." + entry.getKey().split("_")[1] + ".sql");
                    List<String> lines = config.getStringList("Holograms." + entry.getKey().split("_")[1] + ".lines");
                    String linesStats = config.getString("Holograms." + entry.getKey().split("_")[1] + ".linesStats");

                    if (!stats) {
                        continue;
                    }

                    Hologram h = entry.getValue();
                    h.clearLines();

                    MySQL.loadTop(query);

                    for (String s : lines) {
                        h.appendTextLine(s.replace("&", "§"));
                    }

                    int i = 1;
                    for (Map.Entry<String, String> s : MySQL.top.entrySet()) {
                        h.appendTextLine(linesStats
                                .replace("%number%", String.valueOf(i))
                                .replace("%player%", PexFormatter.getPrefix(s.getKey()) + s.getKey())
                                .replace("%value%", s.getValue())
                                .replace("%time%", ru.elliot789.eapi.minigames.utils.Utils.getTimeMsS(Integer.parseInt(s.getValue())))
                                .replace("&", "§"));
                        i++;
                    }
                }
            }
        }.runTask(Settings.plugin);
    }

    static void updateOnline() {
        new BukkitRunnable() {
            @Override
            public void run() {
                if (!Settings.online) return;
                for (Map.Entry<String, Hologram> entry : Settings.holograms.entrySet()) {

                    FileConfiguration config = Settings.holograms_cfg.get(entry.getKey().split("_")[0]);
                    List<String> lines = config.getStringList("Holograms." + entry.getKey().split("_")[1] + ".lines");
                    String server = config.getString("Holograms." + entry.getKey().split("_")[1] + ".online");

                    if (server == null) continue;

                    Hologram h = entry.getValue();
                    h.clearLines();
                    for (String s : lines) {
                        h.appendTextLine(s.replace("&", "§").replace("%online%", String.valueOf(Gui.serversonline.get(server))));
                    }
                }
            }
        }.runTask(Settings.plugin);
    }
}
